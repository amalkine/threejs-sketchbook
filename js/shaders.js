(function(){

  /* Scene */
  var scene = new THREE.Scene();

  /* Material */
  var attributes = {
    displacement: {
      type: 'f',
      value: []
    }
  };

  var uniforms = {
    amplitude: {
      type: 'f',
      value: 0
    }
  };

  var material = new THREE.ShaderMaterial({
    uniforms: uniforms,
    // attributes: attributes,
    vertexShader: document.getElementById('vertexshader').innerHTML,
    fragmentShader: document.getElementById('fragmentshader').innerHTML
  });


  /* Objects */
  var geometry = new THREE.SphereBufferGeometry( 50, 32, 32 );
  console.log("?", geometry);


  /* Shader Attributes */
  var vertexCount = geometry.attributes.position.count;
  console.log("?", vertexCount);
  var displacement = new Float32Array( vertexCount );
  for( var v = 0; v < vertexCount; v++ ) {
    displacement[v] = Math.random() * 30;
  }
  geometry.addAttribute('displacement', new THREE.BufferAttribute( displacement, 1 ) );

  var sphere = new THREE.Mesh( geometry, material );
  scene.add( sphere );




  /* Camera */
  var camera = new THREE.PerspectiveCamera( 75, window.innerWidth / window.innerHeight, 0.1, 1000 );
  camera.position.z = 300;


  /* WebGL Renderer */
  var renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth, window.innerHeight );
  document.body.appendChild( renderer.domElement );

  var frame = 0;
  (function render() {
    uniforms.amplitude.value = Math.sin(frame);
    frame += 0.1;
    renderer.render( scene, camera );
    requestAnimationFrame( render );
  })();

  /* Viewport */
  window.addEventListener( 'resize', function(){
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize( window.innerWidth, window.innerHeight );
  }, false );


  /* Orbit Controls */
  controls = new THREE.OrbitControls( camera, renderer.domElement );

})();