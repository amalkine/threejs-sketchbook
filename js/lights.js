(function(){

  /* Scene */
  var scene = window.scene = new THREE.Scene();


  /* Objects */
  var material = new THREE.MeshLambertMaterial( { color: 0x1393D0 } );

  var floor = new THREE.Mesh( new THREE.CubeGeometry( 60, 1, 60 ), material );
  floor.castShadow = true;
  floor.receiveShadow = true;
  floor.position.set( 0, -15, 0 );

  var cube = new THREE.Mesh( new THREE.CubeGeometry( 20, 10, 20 ), material );
  cube.castShadow = true;
  cube.receiveShadow = true;
  cube.animate = function() {
    cube.rotation.y += 0.01;
  };

  scene.add( cube, floor )


  /* Camera */
  var camera = window.camera = new THREE.PerspectiveCamera( 60, (window.innerWidth/2) / (window.innerHeight/2), 0.1, 1000 );
  camera.position.set( -30, 30, 40 );
  camera.lookAt( new THREE.Vector3( 0, 0, 0 ) );


  /* Lights */
  // var ambiance = new THREE.AmbientLight( 0x404040 );
  // scene.add( ambiance );

  var spot_light = new THREE.SpotLight( 0xffffff, 2 );
  spot_light.position.set( 30, 40, 30 );

  spot_light.castShadow = true;

  spot_light.shadowMapWidth = spot_light.shadowMapHeight = 768;
  spot_light.shadowDarkness = 0.5;

  scene.add( spot_light );

  // var spot_lightHelper = new THREE.SpotLightHelper( spot_light );
  // scene.add( spot_lightHelper );

  /* WebGL Renderer */
  var renderer = new THREE.WebGLRenderer();
  renderer.shadowMapEnabled = true;
  renderer.shadowMapType = THREE.PCFSoftShadowMap;
  renderer.setSize( window.innerWidth/2, window.innerHeight/2 );
  document.body.appendChild( renderer.domElement );

  (function render() {

    scene.traverse(function( node ) {
      if( typeof node.animate === 'function' ) {
        node.animate();
      }
    });

    requestAnimationFrame( render );
    renderer.render( scene, camera );

  })();

  /* Controls */
  controls = new THREE.OrbitControls( camera, renderer.domElement );

})();