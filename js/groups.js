(function(){

  /* Scene */
  var scene = new THREE.Scene();


  /* Camera */
  var camera = new THREE.PerspectiveCamera( 75, (window.innerWidth/2) / (window.innerHeight/2), 0.1, 1000 );
  camera.position.z = 5;


  /* Lights */
  var ambiance = new THREE.AmbientLight( 0x404040 );
  scene.add( ambiance );

  var point_light_above = new THREE.PointLight( 0xffffbb, 1.2, 100 );
  point_light_above.position.set( 0, 3, 0 );
  scene.add( point_light_above );

  var point_light_below = new THREE.PointLight( 0xffffbb, 0.6, 100 );
  point_light_below.position.set( 0, -1, 1 );
  scene.add( point_light_below );


  /* Objects */
  var group = new THREE.Group();
  group.animate = function() {
    group.rotation.x += 0.04;
    group.rotation.y += 0.04;
    group.rotation.z += 0.04;
  }
  scene.add( group );

  var geometry = new THREE.BoxGeometry( 1, 1, 1 );
  var material = new THREE.MeshLambertMaterial( { color: 0x1393D0 } );

  function build_cube() {
    var cube = new THREE.Mesh( geometry, material );
    cube.animate = function() {
      cube.rotation.x += 0.04;
      cube.rotation.y += 0.04;
      cube.rotation.z += 0.04;
    }
    group.add( cube );
    return cube;
  }

  var cube_1 = build_cube();
  cube_1.position.x = -1;
  var cube_2 = build_cube();

  cube_2.position.x = 1;


  /* WebGL Renderer */
  var renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth/2, window.innerHeight/2 );
  document.body.appendChild( renderer.domElement );

  (function render() {

    cube_1.animate();
    cube_2.animate();

    requestAnimationFrame( render );
    renderer.render( scene, camera );
    group.animate();

  })();

})();