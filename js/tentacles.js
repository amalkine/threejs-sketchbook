(function(){

  /* Scene */
  var scene = window.scene = new THREE.Scene();
  var fog = new THREE.FogExp2( 0x000000, 0.1 );
  scene.fog = fog;


  /* Objects */
  var link_size = 0.25;
  var root = new THREE.Object3D;
  var box = new THREE.BoxGeometry( link_size, link_size, link_size );
  var cylinder = new THREE.CylinderGeometry( ( link_size / 1.1 ), link_size, ( link_size * 1.5 ), 18, 1, false );
  var material = new THREE.MeshLambertMaterial( { color: 0x1393D0, fog: true } );

  function build_object( geometry, objects ) {
    var obj = new THREE.Object3D();
    var obj_mesh = new THREE.Mesh( geometry, material );
    obj_mesh.castShadow = true;
    obj_mesh.receiveShadow = true;
    obj_mesh.rotation.x = 90 * (Math.PI/180);
    obj.add( obj_mesh );
    obj.animate = function() {
      obj.rotation.x += 0.0001;
      obj.rotation.y += 0.0001;
      obj.rotation.z += 0.0001;
    }
    var target = objects.length ? objects[ objects.length - 1 ] : root;
    target.add( obj );

    obj.position.z = ( link_size * 1.5 ) - ( link_size/6 );
    obj.opacity = 1 - ( 0.1 * objects.length );

    objects.push(obj);
    return obj;
  }

  function build_tentacle() {
    var tentacle = [];
    var root = build_object( cylinder, tentacle );
    for (var i = 0; i < 100; i++) {
      tentacle.push( build_object( cylinder, tentacle ) );
    };
    return( tentacle );
  }

  var t1 = build_tentacle();
  var t2 = build_tentacle();
  t2[0].rotation.y = 90 * (Math.PI/180);
  var t3 = build_tentacle();
  t3[0].rotation.y = 180 * (Math.PI/180);
  var t4 = build_tentacle();
  t4[0].rotation.y = 270 * (Math.PI/180);
  var t5 = build_tentacle();
  t5[0].rotation.x = 90 * (Math.PI/180);
  var t6 = build_tentacle();
  t6[0].rotation.x = 270 * (Math.PI/180);

  var sph = new THREE.Mesh( new THREE.SphereGeometry( 0.75, 32, 32 ), material );
  sph.position.z = 0.333333;
  root.add( sph );

  root.animate = function() {
    root.rotation.x += 0.006;
    root.rotation.y += 0.006;
    root.rotation.z += 0.006;
  }
  scene.add( root );


  /* Camera */
  var camera = new THREE.PerspectiveCamera( 75, (window.innerWidth/2) / (window.innerHeight/2), 0.1, 1000 );
  camera.position.z = 3;


  /* Lights */
  var ambiance = new THREE.AmbientLight( 0x404040 );
  scene.add( ambiance );

  var point_light_above = new THREE.PointLight( 0xffffbb, 1.2, 100 );
  point_light_above.position.set( 0, 3, 0 );
  scene.add( point_light_above );


  /* WebGL Renderer */
  var renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth/2, window.innerHeight/2 );
  document.body.appendChild( renderer.domElement );

  (function render() {

    scene.traverse(function( node ) {
      if( typeof node.animate === 'function' ) {
        node.animate();
      }
    });

    requestAnimationFrame( render );
    renderer.render( scene, camera );

  })();

  /* Controls */
  controls = new THREE.OrbitControls( camera, renderer.domElement );

})();