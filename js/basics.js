(function(){

  /* Scene */
  var scene = new THREE.Scene();


  /* Camera */
  var camera = new THREE.PerspectiveCamera( 75, (window.innerWidth/2) / (window.innerHeight/2), 0.1, 1000 );
  camera.position.z = 5;


  /* Objects */
  var geometry = new THREE.BoxGeometry( 1, 1, 1 );
  var material = new THREE.MeshBasicMaterial( { color: 0xECECEC } );
  var cube = new THREE.Mesh( geometry, material );
  scene.add( cube );

  var cube_wireframe = new THREE.WireframeHelper( cube, 0x1393D0 );
  scene.add( cube_wireframe );

  function animateCube(){
    cube.rotation.x += 0.04;
    cube.rotation.y += 0.04;
  }


  /* WebGL Renderer */
  var renderer = new THREE.WebGLRenderer();
  renderer.setSize( window.innerWidth/2, window.innerHeight/2 );
  document.body.appendChild( renderer.domElement );

  (function render() {

    animateCube();

    requestAnimationFrame( render );
    renderer.render( scene, camera );

  })();

})();